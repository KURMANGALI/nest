import { News } from '../../news/news.interface';
import { Comment } from '../../news/comments/comments.interface';

export const renderNewsDetail = (
  news: News,
  comments: Comment[] | string,
): string => {
  const commentsIsNotExist = typeof comments === 'string';
  return `
        <div class="container">
            <img src=${news.cover} alt="" />
            <a style="color:#FF0000; text-decoration:none" href='http://localhost:3000/news/all'>
            вернуться к новостям
            </a>
            <h1>${news.title}</h1>
            
            <div>${news.description}</div>
            <div class="text-muted">Автор: ${news.author}</div>
            
            ${
              comments && comments.length > 0 && !commentsIsNotExist
                ? renderNewsComments(comments)
                : 'Нет комментариев'
            }
        </div>
    `;
};

const renderNewsComments = (comments: Comment[]): string => {
  let html = '';
  for (const comment of comments) {
    html += `
            <div class="row">
                <div class="col-lg-1">
                    <img src=${comment?.avatar} class="card-img-top" alt="cat" style="height: 75px; width: 75px; object-fit: cover;  border-radius: 40px;"/>
                </div>
                <div class="col-lg-8">
                    <div>${comment.author}</div>
                    <div>${comment.message}</div>
                </div>
            </div>
        `;
  }

  return html;
};
