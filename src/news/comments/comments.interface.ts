export interface Comment {
  id?: number;
  message: string;
  author: string;
  avatar: string;
}

export type CommentEdit = Partial<Comment>

export type Comments = Record<string | number, Comment[]>;
