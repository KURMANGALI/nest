import { Injectable } from '@nestjs/common';
import { getRandomInt } from '../news.service';
import { Comment, CommentEdit, Comments } from './comments.interface';

@Injectable()
export class CommentsService {
  private readonly comments: Comments = {
    1: [
      {
        id: 1,
        message: 'Первое сообщения',
        author: 'Курмангали',
        avatar:
          'https://phonoteka.org/uploads/posts/2022-04/1651148738_53-phonoteka-org-p-epichnie-oboi-na-rabochii-stol-krasivo-67.jpg',
      },
    ],
  };

  find(newsId: string | number): Comment[] | string {
    if (this.comments[newsId]) {
      return this.comments[newsId];
    }
    return 'Коментарий не найдено!';
  }

  create(newsId: string | number, comment: Comment): Comment {
    const id = getRandomInt(0, 10000) as number;
    if (!this.comments[newsId]) {
      this.comments[newsId] = [];
    }
    this.comments[newsId].push({ id, ...comment });
    return this.comments[newsId][newsId];
  }

  remove(
    newsId: string | number,
    commentId: string | number,
  ): null | Comment[] {
    if (!this.comments[newsId]) {
      return null;
    }
    const indexComment = this.comments[newsId].findIndex(
      (comment) => comment.id === +commentId,
    );
    if (indexComment === -1) {
      return null;
    }
    return this.comments[newsId].splice(indexComment, 1);
  }
  update(
    newsId: string | number,
    commentId: string | number,
    comment: CommentEdit,
  ): boolean | Comment {
    if (!this.comments[newsId]) {
      return false;
    }

    const indexComment = this.comments[newsId].findIndex(
      (comment) => comment.id === +commentId,
    );

    if (indexComment === -1) {
      return false;
    }

    this.comments[newsId][indexComment] = {
      ...this.comments[newsId][indexComment],
      ...comment,
    };

    return this.comments[newsId][indexComment];
  }
}
