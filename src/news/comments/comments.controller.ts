import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Patch,
  Post,
  Put,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { HelperFileLoad } from 'src/utils/HelperFileLoad';
import { Comment, CommentEdit } from './comments.interface';
import { CommentsService } from './comments.service';

const PATH_NEWS = '/static/';
HelperFileLoad.path = PATH_NEWS;

@Controller('comments')
export class CommentsController {
  constructor(private readonly commentsService: CommentsService) {}
  @Get('/:newsId')
  @HttpCode(HttpStatus.OK)
  get(@Param('newsId') newsId: string | number) {
    return this.commentsService.find(newsId);
  }
  @Post('/:newsId')
  @HttpCode(HttpStatus.CREATED)
  @UseInterceptors(
    FileInterceptor('avatar', {
      storage: diskStorage({
        destination: HelperFileLoad.destinationPath,
        filename: HelperFileLoad.customFileName,
      }),
    }),
  )
  create(
    @Param('newsId') newsId: string | number,
    @Body() comment: Comment,
    @UploadedFile() avatar: Express.Multer.File,
  ) {
    if (avatar?.filename) {
      comment.avatar = PATH_NEWS + avatar.filename;
    }
    return this.commentsService.create(newsId, comment);
  }
  @Delete('/:newsId/:commentId')
  @HttpCode(HttpStatus.OK)
  remove(
    @Param('newsId') newsId: string | number,
    @Param('commentId') commentId: string | number,
  ) {
    return this.commentsService.remove(newsId, commentId);
  }
  @Patch('/:newsId/:commentId')
  @HttpCode(HttpStatus.OK)
  update(
    @Param('newsId') newsId: string | number,
    @Param('commentId') commentId: string | number,
    @Body() comment: CommentEdit,
  ) {
    return this.commentsService.update(newsId, commentId, comment);
  }
}
