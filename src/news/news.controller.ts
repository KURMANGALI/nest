import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Patch,
  Post,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { renderNewsAll } from 'src/view/news/news-all';
import { renderNewsDetail } from 'src/view/news/news-details';
import { renderTemlate } from 'src/view/template';
import { CommentsService } from './comments/comments.service';
import { CreateNewsDto } from './create.user.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { News, NewsEdit } from './news.interface';
import { NewsService } from './news.service';
import { diskStorage } from 'multer';
import { HelperFileLoad } from 'src/utils/HelperFileLoad';

const PATH_NEWS = '/static/'
HelperFileLoad.path = PATH_NEWS

@Controller('news')
export class NewsController {
  constructor(
    private readonly newsService: NewsService,
    private readonly commentsService: CommentsService,
  ) {}
  @Get()
  @HttpCode(HttpStatus.OK)
  getNews() {
    return this.newsService.getAllNews();
  }

  @Get('/all')
  @HttpCode(HttpStatus.OK)
  getAllView() {
    const news = this.newsService.getAllNews();
    const content = renderNewsAll(news);

    return renderTemlate(content, {
      title: 'Список новостей',
      description: 'Самые интересные новости на свете!',
    });
  }

  @Get('/view/:id')
  @HttpCode(HttpStatus.OK)
  getDetailView(@Param('id') id: string) {
    const news = this.newsService.find(id);
    const comments = this.commentsService.find(id);

    const content = renderNewsDetail(news, comments);

    return renderTemlate(content, {
      title: news.title,
      description: news.description,
    });
  }

  @Get('/:id')
  @HttpCode(HttpStatus.OK)
  get(@Param('id') id: number) {
    const news = this.newsService.find(id);
    const comments = this.commentsService.find(id);

    return {
      ...news,
      comments,
    };
  }
  @Post()
  @UseInterceptors(
    FileInterceptor('cover', {
      storage: diskStorage({
        destination: HelperFileLoad.destinationPath,
        filename: HelperFileLoad.customFileName,
      }),
    }),
  )
  @HttpCode(HttpStatus.CREATED)
  create(
    @Body() createNewsDto: News,
    @UploadedFile() cover: Express.Multer.File,
  ) {
    if (cover?.filename) {
      createNewsDto.cover = PATH_NEWS + cover.filename;
    }
    return this.newsService.create(createNewsDto);
  }

  @Delete('/:id')
  @HttpCode(HttpStatus.OK)
  remove(@Param('id') id: number) {
    const isRemoved = this.newsService.remove(id);

    if (isRemoved) {
      return 'Новость удалена';
    }
    return 'Передан неверный идентификатор';
  }
  @Patch('/:id')
  @HttpCode(HttpStatus.OK)
  edit(@Param('id') id: number, @Body() news: NewsEdit) {
    return this.newsService.edit(id, news);
  }
}
